FROM jenkins

USER root
RUN apt-get update
RUN apt-get install -y apt-transport-https ca-certificates
RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
RUN echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > /etc/apt/sources.list.d/docker.list
RUN apt-get update
RUN apt-get install -y docker-engine=1.9.1-0~trusty sudo
RUN usermod -aG docker jenkins
RUN apt-get install -y build-essential python-yaml python-paramiko python-jinja2 python-httplib2 python-setuptools python-pip python2.7 libpython2.7-dev
RUN git clone https://github.com/ansible/ansible /tmp/ansible
WORKDIR /tmp/ansible
RUN git submodule update --init --recursive && make && make install
RUN mkdir /var/log/jenkins
RUN chown -R  jenkins:jenkins /var/log/jenkins

USER jenkins

# Install RVM
RUN gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
RUN \curl -sSL https://get.rvm.io | bash

ENV JENKINS_OPTS="--logfile=/var/log/jenkins/jenkins.log"
